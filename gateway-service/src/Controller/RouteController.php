<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RouteController extends AbstractController
{
    private HttpClientInterface $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    #[Route('/{req}', name: 'route', requirements: ['req' => '.+'])]
    public function index(Request $request): Response
    {
        $pathElements = array_filter(explode('/',$request->getPathInfo()));

        $serverName = array_shift($pathElements);
        if($serverName === 'security') {
            $url = 'http://security-service/' . implode('/', $pathElements);
            $clientResponse = $this->client->request($request->getMethod(), $url, ['body' => $request->getContent()]);
            return new Response($clientResponse->getContent(), $clientResponse->getStatusCode(), $clientResponse->getHeaders());
        }

        return $this->json([
            'message' => array_filter(explode('/',$request->getPathInfo())),
            'path' => 'src/Controller/RouteController.php',
        ]);
    }
}
