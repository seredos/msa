import React from "react";
import {
    Button,
    Checkbox,
    Dialog, DialogActions,
    DialogContent,
    DialogTitle,
    FormControlLabel,
    FormGroup,
    TextField
} from "@mui/material";
import axios from "axios";

type ClientDialogProps  = {
    row: any;
    icon: React.ReactNode;
    method: string;
    reload: () => void;
}

export const ClientDialog: React.FC<ClientDialogProps> = ({row, icon, method, reload}:ClientDialogProps) => {
    const [open, setOpen] = React.useState(false);
    const [identifier, setIdentifier] = React.useState(row.identifier ?? '');
    const [name, setName] = React.useState(row.name ?? '');
    const [redirectUri, setRedirectUri] = React.useState(row.redirectUri ?? '');
    const [confidential, setConfidential] = React.useState(row.confidential ?? false);

    function editClient() {
        setOpen(true);
    }

    function handleClose() {
        setOpen(false);
    }

    function handleSave() {
        console.log(method);
        console.log(identifier);
        if(method == 'post') {
            axios.post('http://127.0.0.1:8082/security/client', {
                identifier: identifier,
                name: name,
                redirectUri: redirectUri,
                confidential: confidential
            }).then(res => {
            });
        } else {
            axios.put('http://127.0.0.1:8082/security/client', {
                identifier: identifier,
                name: name,
                redirectUri: redirectUri,
                confidential: confidential
            }).then(res => {
            });
        }

        reload();
        setOpen(false);
    }

    function handleIdentifierChange(e: React.ChangeEvent<HTMLInputElement>) { setIdentifier(e.target.value); }
    function handleNameChange(e: React.ChangeEvent<HTMLInputElement>) { setName(e.target.value); }
    function handleRedirectUriChange(e: React.ChangeEvent<HTMLInputElement>) { setRedirectUri(e.target.value); }
    function handleConfidentialChange() { setConfidential(!confidential); }

    return (
        <>
            <Button variant="outlined" startIcon={icon} onClick={editClient}/>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Edit Client</DialogTitle>
                <DialogContent>
                    <FormGroup>
                        <TextField label={"Identifier"} value={identifier} onChange={handleIdentifierChange} disabled={row.id > 0}/>
                        <TextField label={"Name"} value={name} onChange={handleNameChange}/>
                        <TextField label={"Redirect URI"} value={redirectUri} onChange={handleRedirectUriChange}/>
                        <FormControlLabel
                            label="Confidential"
                            control={<Checkbox checked={confidential} onChange={handleConfidentialChange}/>}/>
                    </FormGroup>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={handleSave}>Save</Button>
                </DialogActions>
            </Dialog>
        </>
    );
}