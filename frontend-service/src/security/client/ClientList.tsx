import React, {useEffect, useState} from "react";
import {
    DataGrid,
    GridColDef,
    GridRowsProp,
    GridToolbar
} from "@mui/x-data-grid";
import {Button} from "@mui/material";
import {Create, Delete, Edit} from "@mui/icons-material";
import axios from "axios";
import {ClientDialog} from "./ClientDialog";

type ClientProps = {}

export const ClientList: React.FC<ClientProps> = ({}: ClientProps) => {
    function deleteClient(identifier: string) {
        console.log(identifier);
    }

    function reload() {
        setRows([]);
    }

    const columns: GridColDef[] = [
        { field: 'actions', headerName: 'Actions', width: 150, renderCell: (params) => {
            return (<div>
                <Button variant="outlined" startIcon={<Delete />} onClick={() => {
                    deleteClient(params.row.identifier);
                }}/>
                <ClientDialog row={params.row} icon={<Edit />} method={"put"} reload={reload}/>
            </div>);
            }},
        { field: 'identifier', headerName: 'Identifier', width: 150},
        { field: 'name', headerName: 'Name', width: 150 },
        { field: 'redirectUri', headerName: 'redirect uri', width: 150 },
        { field: 'confidential', headerName: 'confidential', width: 150 },
    ];

    const [rows,setRows] = useState<GridRowsProp>([]);

    useEffect(() => {
        if(!rows.length) {
            axios.get('http://127.0.0.1:8082/security/client').then(res => {
                setRows(res.data);
            });
        }
    }, [rows]);

    return (
        <div>
            <ClientDialog row={{}} icon={<Create />} method={"post"} reload={reload}/>
            <div style={{ height: 600, width: '100%' }}>
                <DataGrid rows={rows}
                          columns={columns}
                          checkboxSelection={true}
                          editMode={"row"}
                          components={{
                            Toolbar: GridToolbar,
                          }}/>
            </div>
        </div>
    );
}