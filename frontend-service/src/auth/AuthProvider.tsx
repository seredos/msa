import React from "react";
import axios from "axios";

type AuthProviderProps = {
    authUrl: string,
    tokenUrl: string,
    redirectUri: string,
    clientId: string,
    children: any
}

export const AuthProvider: React.FC<AuthProviderProps> = (
    {
        authUrl,
        tokenUrl,
        redirectUri,
        clientId,
        children
    }: AuthProviderProps) => {
    let token = localStorage.getItem('token');
    const urlParams = new URLSearchParams(window.location.search);
    const code = urlParams.get('code');

    if(code) {
        const formData = new FormData();
        formData.append('grant_type', 'authorization_code');
        formData.append('client_id', clientId);
        formData.append('redirect_uri', redirectUri);
        formData.append('code', code);

        axios.post(tokenUrl, formData).then(res => {
            localStorage.setItem('token', res.data.access_token);
            token = res.data.access_token;
        }).catch(res => {
            console.log(res);
        });
    }

    if(!token) {
        document.location.href = authUrl + "?response_type=code&client_id=" + clientId + "&redirect_uri=" + redirectUri
    }

    return (
        <div>
            {authUrl}
            {children}
        </div>
    );
}