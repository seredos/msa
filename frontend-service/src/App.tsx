import React from 'react';
import logo from './logo.svg';
import './App.css';
import {AuthProvider} from "./auth/AuthProvider";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import {Button} from "@mui/material";
import {ClientList} from "./security/client/ClientList";

function Home() {
    return (
        <div>
            home
            <Button variant="contained">Hello World</Button>
        </div>
    );
}

function App() {
  return (
    <div className="App">
      {/*<header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>*/}
        <Router>
            <Routes>
                <Route path={"/"} element={<Home/>}/>
                <Route path={"/security/client/list"} element={<ClientList/>}/>
            </Routes>
            {/*<Route path={"/login"}>
                <AuthProvider authUrl={"http://127.0.0.1:8080/o/auth"}
                              tokenUrl={"http://127.0.0.1:8080/o/auth/token"}
                              clientId={"default"}
                              redirectUri={"http://127.0.0.1/"}>
                    <div>
                        test login
                    </div>
                </AuthProvider>
            </Route>*/}
        </Router>
    </div>
  );
}

export default App;
