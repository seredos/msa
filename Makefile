include auth-service/Makefile
include security-service/Makefile
include frontend-service/Makefile
include gateway-service/Makefile
include ci/prod/Makefile

build: build-auth-service build-security-service build-gateway-service build-frontend-service
start: start-auth-service start-security-service start-gateway-service start-frontend-service
test: test-auth-service test-security-service test-gateway-service test-frontend-service
build-prod: build-auth-service-prod build-security-service-prod build-gateway-service-prod build-frontend-service-prod
build-images: build-auth-service-images build-security-service-images build-gateway-service-images build-frontend-service-image
#build: build-auth-service build-frontend-service build-security-service build-gateway-service
#build-prod: build-auth-service-prod build-frontend-service-prod build-security-service-prod build-gateway-service-prod
#install: install-auth-service
#start: start-auth-service start-frontend-service start-security-service start-gateway-service
#test: test-auth-service test-frontend-service
#build-prod-images: build-auth-service-prod-images build-frontend-service-prod-image build-security-service-prod-images build-gateway-service-prod-images