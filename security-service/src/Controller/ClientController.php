<?php

namespace App\Controller;

use App\Message\ClientMessage;
use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use App\Entity\Client;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Serializer\SerializerInterface;

class ClientController extends AbstractController
{
    private ClientRepository $clientRepository;
    private SerializerInterface $serializer;
    private MessageBusInterface $bus;

    public function __construct(ClientRepository $clientRepository, SerializerInterface $serializer, MessageBusInterface $bus)
    {
        $this->clientRepository = $clientRepository;
        $this->serializer = $serializer;
        $this->bus = $bus;
    }

    #[Route('/client', name: 'options_client', methods: ['OPTIONS'])]
    public function options(): Response
    {
        return new Response();
    }

    /**
     * @OA\Response(
     *     response=200,
     *     description="Returns a list of clients",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Client::class))
     *     )
     * )
     * @OA\Tag(name="client")
     */
    #[Route('/client', name: 'list_client', methods: ['GET'])]
    public function list(): Response
    {
        $clients = $this->clientRepository->findAll();
        return $this->json($clients);
    }

    /**
     * @OA\Post(
     *     requestBody=@OA\RequestBody(
     *          @Model(type=Client::class)
     *     )
     * )
     * @OA\Response(
     *     response=200,
     *     description="create a new client",
     *     @OA\JsonContent(
     *        ref=@Model(type=Client::class)
     *     )
     * )
     * @OA\Tag(name="client")
     */
    #[Route('/client', name: 'create_client', methods: ['POST'])]
    public function create(Request $request): Response {
        /** @var $client Client*/
        $client = $this->serializer->deserialize($request->getContent(), Client::class, 'json');

        $dbClient = $this->clientRepository->findOneBy(['identifier' => $client->getIdentifier()]);

        if(!$dbClient) {
            $clientMessage = new ClientMessage();
            $clientMessage->setIdentifier($client->getIdentifier())
                ->setName($client->getName())
                ->setRedirectUri($client->getRedirectUri())
                ->setConfidential($client->getConfidential());

            $this->bus->dispatch($clientMessage);

            $dbClient = new Client();
            $dbClient->setIdentifier($client->getIdentifier())
                ->setName($client->getName())
                ->setRedirectUri($client->getRedirectUri())
                ->setConfidential($client->getConfidential());

            $this->clientRepository->save($dbClient);
        }

        return new Response($this->serializer->serialize($client, 'json'));
    }

    /**
     * @OA\Put(
     *     requestBody=@OA\RequestBody(
     *          @Model(type=Client::class)
     *     )
     * )
     * @OA\Response(
     *     response=200,
     *     description="update a client",
     *     @OA\JsonContent(
     *        ref=@Model(type=Client::class)
     *     )
     * )
     * @OA\Tag(name="client")
     */
    #[Route('/client', name: 'update_client', methods: ['PUT'])]
    public function update(Request $request): Response {
        /** @var $client Client*/
        $client = $this->serializer->deserialize($request->getContent(), Client::class, 'json');

        $dbClient = $this->clientRepository->findOneBy(['identifier' => $client->getIdentifier()]);

        if($dbClient) {
            $clientMessage = new ClientMessage();
            $clientMessage->setIdentifier($client->getIdentifier())
                ->setName($client->getName())
                ->setRedirectUri($client->getRedirectUri())
                ->setConfidential($client->getConfidential());

            $this->bus->dispatch($clientMessage);

            $dbClient->setName($client->getName())
                ->setRedirectUri($client->getRedirectUri())
                ->setConfidential($client->getConfidential());

            $this->clientRepository->save($dbClient);
        }

        return new Response($this->serializer->serialize($client, 'json'));
    }

    /**
     * @OA\Delete(
     *     requestBody=@OA\RequestBody(
     *          @Model(type=Client::class)
     *     )
     * )
     * @OA\Response(
     *     response=200,
     *     description="update a client",
     *     @OA\JsonContent(
     *        ref=@Model(type=Client::class)
     *     )
     * )
     * @OA\Tag(name="client")
     */
    #[Route('/client', name: 'delete_client', methods: ['DELETE'])]
    public function delete(Request $request): Response {
        /** @var $client Client*/
        $client = $this->serializer->deserialize($request->getContent(), Client::class, 'json');

        $dbClient = $this->clientRepository->findOneBy(['identifier' => $client->getIdentifier()]);

        $clientMessage = new ClientMessage();
        $clientMessage->setIdentifier($dbClient->getIdentifier())
            ->setName($dbClient->getName())
            ->setRedirectUri($dbClient->getRedirectUri())
            ->setConfidential($dbClient->getConfidential())
            ->setDelete(true);

        $this->bus->dispatch($clientMessage);

        $this->clientRepository->deleteClient($client->getIdentifier());

        return new Response();
    }
}
