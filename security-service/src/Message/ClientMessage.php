<?php

namespace App\Message;

final class ClientMessage
{
    private string $identifier;
    private string $name;
    private string $redirectUri;
    private bool $confidential = false;
    private bool $delete = false;

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return ClientMessage
     */
    public function setIdentifier(string $identifier): ClientMessage
    {
        $this->identifier = $identifier;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ClientMessage
     */
    public function setName(string $name): ClientMessage
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getRedirectUri(): string
    {
        return $this->redirectUri;
    }

    /**
     * @param string $redirectUri
     * @return ClientMessage
     */
    public function setRedirectUri(string $redirectUri): ClientMessage
    {
        $this->redirectUri = $redirectUri;
        return $this;
    }

    /**
     * @return bool
     */
    public function isConfidential(): bool
    {
        return $this->confidential;
    }

    /**
     * @param bool $confidential
     * @return ClientMessage
     */
    public function setConfidential(bool $confidential): ClientMessage
    {
        $this->confidential = $confidential;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDelete(): bool
    {
        return $this->delete;
    }

    /**
     * @param bool $delete
     * @return ClientMessage
     */
    public function setDelete(bool $delete): ClientMessage
    {
        $this->delete = $delete;
        return $this;
    }
}
