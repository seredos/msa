<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

#[ORM\Entity(repositoryClass: ClientRepository::class)]
class Client
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    /**
     * @OA\Property(type="string", maxLength=255)
     * @var string
     */
    #[ORM\Column(type: 'string', length: 255)]
    private string $identifier;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    #[ORM\Column(type: 'string', length: 255)]
    private string $redirectUri;

    /**
     * @OA\Property(type="boolean")
     */
    #[ORM\Column(type: 'boolean')]
    private bool $confidential;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRedirectUri(): ?string
    {
        return $this->redirectUri;
    }

    public function setRedirectUri(string $redirectUri): self
    {
        $this->redirectUri = $redirectUri;

        return $this;
    }

    public function getConfidential(): ?bool
    {
        return $this->confidential;
    }

    public function setConfidential(bool $confidential): self
    {
        $this->confidential = $confidential;

        return $this;
    }
}