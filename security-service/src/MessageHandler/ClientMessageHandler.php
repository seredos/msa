<?php

namespace App\MessageHandler;

use App\Message\ClientMessage;
use App\Repository\ClientRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ClientMessageHandler implements MessageHandlerInterface
{
    private ClientRepository $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function __invoke(ClientMessage $message)
    {
        if(!$message->isDelete()) {
            $this->clientRepository->saveMessage($message);
        } else {
            $this->clientRepository->deleteClient($message->getIdentifier());
        }
    }
}
