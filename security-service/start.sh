#!/bin/bash

php bin/console cache:clear
php bin/console doctrine:database:create --if-not-exists
php bin/console doctrine:migrations:migrate --no-interaction


#php bin/console messenger:consume client &
supervisord
supervisorctl reread
supervisorctl update
supervisorctl start messenger-consume:*

php-fpm