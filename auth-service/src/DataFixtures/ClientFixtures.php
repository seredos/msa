<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ClientFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $client = new Client();
        $client->setIdentifier('default');
        $client->setName('Default');
        $client->setRedirectUri('/');

        $manager->persist($client);

        $manager->flush();
    }
}
