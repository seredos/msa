<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Login;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param User $user
     * @return void
     * @throws UniqueConstraintViolationException
     */
    public function saveUser(User $user) {

        $user->setIdentifier(uniqid('', true));
        $pwd = $user->getPassword();
        $user->setPassword(password_hash($pwd, PASSWORD_DEFAULT));

        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
    }

    public function login(Login $login): ?User {
        $user = $this->findOneBy(['eMail' => $login->getIdentifier()]);
        if($user && password_verify($login->getPassword(), $user->getPassword())) {
            return $user;
        }
        return null;
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
