<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Client;
use App\Message\ClientMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository implements ClientRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    /**
     * @throws OAuthServerException
     */
    public function getClientEntity($clientIdentifier): ClientEntityInterface
    {
        $client = $this->findOneBy(['identifier' => $clientIdentifier]);
        if(!$client) {
            throw new OAuthServerException('Client authentication failed', 4, 'invalid_client', 401);
        }
        return $client;
    }

    public function validateClient($clientIdentifier, $clientSecret, $grantType): bool
    {
        return true;
    }

    public function saveMessage(ClientMessage $message) {
        $client = $this->findOneBy(['identifier' => $message->getIdentifier()]);
        if(!$client) {
            $client = new Client();
            $client->setIdentifier($message->getIdentifier());
        }
        $client->setName($message->getName())
            ->setRedirectUri($message->getRedirectUri())
            ->setConfidential($message->isConfidential());

        $this->getEntityManager()->persist($client);
        $this->getEntityManager()->flush();
    }

    public function deleteClient(string $identifier) {
        $client = $this->findOneBy(['identifier' => $identifier]);
        $this->getEntityManager()->remove($client);
        $this->getEntityManager()->flush();
    }
}