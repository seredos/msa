<?php
declare(strict_types=1);

namespace App\Command;

use App\Message\ClientMessage;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(
    name: 'install:client',
    description: 'Install a new OAuth client',
)]
class InstallClientCommand extends Command
{
    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus, string $name = null)
    {
        parent::__construct($name);
        $this->bus = $bus;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('identifier', InputArgument::REQUIRED, 'the client identifier')
            ->addOption('name', null, InputOption::VALUE_REQUIRED, 'the client name')
            ->addOption('redirectUri', null, InputOption::VALUE_REQUIRED, 'the client redirect uri')
            ->addOption('confidential', null, InputOption::VALUE_NONE, 'mark client as confidential')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $identifier = $input->getArgument('identifier');
        $name = $input->getOption('name');
        $redirectUri = $input->getOption('redirectUri') ?? '/';
        $confidential = $input->getOption('confidential') ?? false;
        if(!$name)
            $name = $identifier;

        $client = new ClientMessage();
        $client->setIdentifier($identifier)
            ->setName($name)
            ->setRedirectUri($redirectUri)
            ->setConfidential($confidential);

        $this->bus->dispatch($client);
        $io->success('client was successfully sent via the message bus');

        return Command::SUCCESS;
    }
}
