<?php

namespace App\Entity;

class Login
{
    private string $identifier;
    private string $password;

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return Login
     */
    public function setIdentifier(string $identifier): Login
    {
        $this->identifier = $identifier;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Login
     */
    public function setPassword(string $password): Login
    {
        $this->password = $password;
        return $this;
    }
}