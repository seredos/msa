<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Login;
use App\Entity\User;
use App\Repository\AuthCodeRepository;
use App\Repository\RefreshTokenRepository;
use App\Repository\UserRepository;
use App\Type\LoginType;
use App\Type\RegisterType;
use DateInterval;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AuthCodeGrant;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OAuthController extends AbstractController
{
    private UserRepository $userRepository;
    private AuthorizationServer $server;
    private PsrHttpFactory $httpFactory;
    private HttpFoundationFactory $symfonyFactory;

    /**
     * @throws Exception
     */
    public function __construct(AuthorizationServer    $server,
                                UserRepository         $userRepository,
                                AuthCodeRepository     $authCodeRepository,
                                RefreshTokenRepository $refreshTokenRepository,
                                PsrHttpFactory         $httpFactory,
                                HttpFoundationFactory  $symfonyFactory)
    {
        $this->userRepository = $userRepository;
        $this->server = $server;
        $this->httpFactory = $httpFactory;
        $this->symfonyFactory = $symfonyFactory;

        $authCodeGrant = new AuthCodeGrant($authCodeRepository, $refreshTokenRepository, new DateInterval('P1D'));

        $this->server->enableGrantType($authCodeGrant);
    }

    #[Route('/o/auth/token', name: 'o_auth_token', methods: ['POST'])]
    public function token(Request $request): Response
    {
        $psrRequest = $this->httpFactory->createRequest($request);
        $psrResponse = $this->httpFactory->createResponse(new Response());

        $psrResponse = $this->server->respondToAccessTokenRequest($psrRequest, $psrResponse);
        return $this->symfonyFactory->createResponse($psrResponse);
    }

    /**
     * @throws OAuthServerException
     */
    #[Route('/o/auth', name: 'o_auth')]
    public function index(Request $request): Response
    {
        $psrRequest = $this->httpFactory->createRequest($request);
        try {
            $authRequest = $this->server->validateAuthorizationRequest($psrRequest);
        } catch (OAuthServerException $e) {
            if($e->getCode() === 4){
                return $this->render('o_auth/invalid_client.html.twig');
            }else
                throw $e;
        }

        $login = new Login();
        $loginForm = $this->createForm(LoginType::class, $login);

        $register = new User();
        $registerForm = $this->createForm(RegisterType::class, $register);

        $registerForm->handleRequest($request);
        $loginForm->handleRequest($request);

        $authUser = null;
        if($registerForm->isSubmitted() && $registerForm->isValid()) {
            $register = $registerForm->getData();

            try {
                $this->userRepository->saveUser($register);
            } catch (UniqueConstraintViolationException) {
                $registerForm->addError(new FormError('user already exists'));
                $register = null;
            }
            $authUser = $register;
        }

        if($loginForm->isSubmitted() && $loginForm->isValid()) {
            $login = $loginForm->getData();
            $authUser = $this->userRepository->login($login);

            if(!$authUser) {
                $loginForm->addError(new FormError('invalid username or password'));
            }
        }

        if($authUser) {
            $authRequest->setUser($authUser);
            $authRequest->setAuthorizationApproved(true);

            $psrResponse = $this->httpFactory->createResponse(new Response());
            return $this->symfonyFactory->createResponse($this->server->completeAuthorizationRequest($authRequest, $psrResponse));
        }

        return $this->render('o_auth/index.html.twig', [
            'login' => $loginForm->createView(),
            'register' => $registerForm->createView()
        ]);
    }
}
