#!/bin/bash

PRIVATE_KEY="${AUTH_PRIVATE_KEY:-"/auth/private.key"}"
CLIENT="${AUTH_CLIENT:-"https://frontend.sith-force.eu/"}"

KEY_DIR="$(dirname "${PRIVATE_KEY}")"

export AUTH_PRIVATE_KEY=$PRIVATE_KEY

mkdir -p "$KEY_DIR"

if [ ! -f "$PRIVATE_KEY" ]; then
  openssl genrsa -out "$PRIVATE_KEY" 2048
fi

chmod 660 "$PRIVATE_KEY"
chown www-data:www-data "$PRIVATE_KEY"

php bin/console cache:clear
php bin/console doctrine:database:create --if-not-exists
php bin/console doctrine:migrations:migrate --no-interaction

php bin/console install:client system --redirectUri="$CLIENT" --confidential

php bin/console messenger:consume client &

php-fpm