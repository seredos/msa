Auth Service
============

This service provides an OAuth authorization procedure that allows various clients to identify the user.

installation
------------

The following environment variables must be set for the service to operate:

| variable | description | default                                                                     |
|----------|-------------|-----------------------------------------------------------------------------|
|DATABASE_URL|Setting the database connection for the service| mysql://root:secret@database:3306/auth-service?serverVersion=8&charset=utf8 |
|AUTH_PRIVATE_KEY|Defines the private key with which the service generates access tokens. If the key does not exist, it will be generated automatically at startup.| /auth/private.key                                                           |
|MESSENGER_TRANSPORT_DSN|Defines the messenger service through which the service receives client and user information.| redis://redis:6379                                                          |
|AUTH_CLIENT|Defines the URL of the first system client that can use the service.| https://frontend.sith-force.eu/                                             |
