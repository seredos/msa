<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OAuthControllerTest extends WebTestCase
{
    public function testRegisterWithDefaultClient(): void
    {
        $client = static::createClient();
        $client->request('GET', '/o/auth?response_type=code&client_id=default');

        $this->assertResponseIsSuccessful();
        $client->submitForm('register[save]', [
            'register[firstName]' => 'test_user',
            'register[lastName]' => 'test_user',
            'register[eMail]' => 'test_user@web.de',
            'register[password][first]' => 'test',
            'register[password][second]' => 'test'
        ]);

        $this->assertResponseStatusCodeSame(302);
        $this->assertStringStartsWith('/?code=', $client->getResponse()->headers->get('location'));
    }

    public function testWithInvalidClientUri(): void {
        $client = static::createClient();
        $client->request('GET', '/o/auth?response_type=code&client_id=default&redirect_uri=https://www.invalid.com/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('div', 'invalid client');
    }

    public function testWithInvalidClient(): void {
        $client = static::createClient();
        $client->request('GET', '/o/auth?response_type=code&client_id=invalid');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('div', 'invalid client');
    }

    /**
     * @depends testRegisterWithDefaultClient
     * @return void
     */
    public function testRegisterIfUserAlreadyExists(): void {
        $client = static::createClient();
        $client->request('GET', '/o/auth?response_type=code&client_id=default');

        $this->assertResponseIsSuccessful();
        $client->submitForm('register[save]', [
            'register[firstName]' => 'test_user',
            'register[lastName]' => 'test_user',
            'register[eMail]' => 'test_user@web.de',
            'register[password][first]' => 'test',
            'register[password][second]' => 'test'
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('li', 'user already exists');
    }

    /**
     * @depends testRegisterWithDefaultClient
     * @return void
     */
    public function testLoginWithDefaultClient(): void
    {
        $client = static::createClient();
        $client->request('GET', '/o/auth?response_type=code&client_id=default');

        $this->assertResponseIsSuccessful();
        $client->submitForm('login[login]', [
            'login[identifier]' => 'test_user@web.de',
            'login[password]' => 'test'
        ]);

        $this->assertResponseStatusCodeSame(302);
        $this->assertStringStartsWith('/?code=', $client->getResponse()->headers->get('location'));
    }

    /**
     * @depends testRegisterWithDefaultClient
     * @return void
     */
    public function testLoginWithDefaultClientAndInvalidPassword(): void
    {
        $client = static::createClient();
        $client->request('GET', '/o/auth?response_type=code&client_id=default');

        $this->assertResponseIsSuccessful();
        $client->submitForm('login[login]', [
            'login[identifier]' => 'test_user@web.de',
            'login[password]' => 'invalid'
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('li', 'invalid username or password');
    }
}
